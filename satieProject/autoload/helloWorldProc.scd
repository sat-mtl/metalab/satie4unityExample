// USES GLOBAL VARIABLE:   ~satie <required>

// this file defines  a simple process class, which is ONLY used by source nodes that have that have a URI of type:   "process" , e.g:      uri://process  "helloWorld"
// you can define any number of these process clases, each one, corresponding to a particular type of processing  behavoir you define
// any number of instances of a given process class can be created

// About this example
// This process accepts creation arguments and responds to incoming messages by printing messages to the console if the "debug" field is true. No sound generation is defined.

// name the process here
~myProcessName = \helloWorld;

~myProcess = Environment.make({

	// REQUIRED field
	~satieInstance = nil;

	// Not required but useful
	~nodeName = \nil;
	~nodeGroupName = \nil;
	~nodeGroup = nil;
	~debugFlag = 0;


	// REQUIRED functions:  'setup' and 'cleanup'

	// this setup function  can be extended to suit particular needs of process
	~setup = { | self, nodeName, nodeGroupName, args |
		var thisGroup = self.satieInstance.groups[nodeGroupName.asSymbol] ;

		// Not required but useful
		self.nodeName = nodeName;
		self.nodeGroupName = nodeGroupName;
		self.nodeGroup = thisGroup;

		//if (self.debugFlag != 0, {postf("••helloWorld.setup:  Before ARG PARSING: node: %  group: %    args % \n", self.nodeName, self.nodeGroupName, args)};

		// IF there are args, try to parse them and set corresponding environment key value pairs
		if (args != nil,
			{
				if (  args.size > 1,
					{
						if (  (args.size&1)==1,    // odd length,  bad key value format
							{
								warn("helloWorld.setup:  node: %   ignoring args:  expecting key value pair(s)  % \n",  self.nodeName, args);
							},
							// else arg list good to parse
							{
								args.pairsDo({ | key, val |
									self[key.asSymbol] = val;
								});
						});
				});
		});

		// add project-specific code below

		if (self.debugFlag != 0,
			{postf("••helloWorld.setup:  node: %  group: %    args % \n", self.nodeName, self.nodeGroupName, args);});
	};

	// cleanup called when instance is killed
	~cleanup = { | self |

		// add project-specific code below
		if (self.debugFlag != 0, {  postf("••helloWorld.cleanup: clearing synths in node: % group \n", self.nodeName);});
	};

	// end of required  functions



	/*       SatieOSC message handling
	By default, the following OSC messages are generically handled by SatieOSC

	/satie/process/update   processName   azi ele gdb del lpf dst    // sets the arguments to the processes' group
	/satie/process/property processName  key value  -->  // writes the key value pair to the  processes' environment
	/satie/process/state      processName  state  value  -->   // activates or deactivates the processes' group
	/satie/process/set         processName  key value  --> // sets the key value pair to the processes' group
	/satie/process/setvec     processName  key [ val1 .... valN ]  -->  sets the key and values[]  to the processes' group
	/satie/process/eval        processName  handlerName argVec[]   -->  calls the processes' named handler (process[\handlerName])  with zero or more values in argVec[]


	Optional Override handlers
	Except for the 'eval' message, if the process defines handlers that correspond to the the messages above,  these handlers will be called instead.

	Defined below are overide handlers for the following  messages, which are handled by the corresponding process handlers as shown:

	/satie/process/update   -->  handled by process[\update]
	/satie/process/property -->  handled by process[\property]
	/satie/process/state      -->   hadled by  process[\state]
	/satie/process/set         -->  handled by process[\set]
	/satie/process/setvec     -->  handled by process[\setVec]

	*/

	// overrides SatieOSC's 'set' handler
	~set =  {| self, key, value |
		if (self.debugFlag != 0, { postf("••helloWorld.set: key %  value % \n", key, value);});
	};

	// overrides SatieOSC's 'setVec' handler
	~setVec =  {| self, key, vector |
		if (self.debugFlag != 0, { postf("••nearField.setVec: key %  value % \n", key, vector);});
		self.synthPtr.set(key.asSymbol, vector);
	};

	// overrides satieOSC's 'property' handler
	~property =  {| self, key, value |
		if (self.debugFlag != 0, {  postf("••helloWorld.setProperty:  setting key vaue pair:  %:%   to process node %'s environment \n", key, value,  self.nodeName);});

		// project specific code below
		self.put(key.asSymbol, value);
	};

	// overrides satieOSC's 'update' handler
	~setUpdate = { | self, aziDeg, eleDeg, gainDB, delayMs, lpHz, dist |
		if (self.debugFlag != 0, {  postf("••helloWorld.setUpdate: for process node \n", self.nodeName);});
	};


	// overrides satieOSC's 'state' function
	~state =  {| self, value |
		if (self.debugFlag != 0, {  postf("••helloWorld.state: setting state of process node %   to % \n", self.nodeName, value);});
	};


	// CUSTOM handler  that is called on reception of:  /scene/process/eval nodeName doit args[]

	~doit =  {| self, vector |
		if (self.debugFlag != 0, {  postf("••helloWorld.doit: for process node %, with args: % \n", self.nodeName, vector);});
	};

});  // end of environment

// tell Satie to create this process
~satie.makeProcess(~myProcessName, ~myProcess);